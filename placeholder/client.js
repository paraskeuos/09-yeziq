// NE DIRATI - generisan kljuc za Yandex recnik
const YANDEX_API_KEY = 'trnsl.1.1.20200322T213114Z.8d17d03d4e02fa43.c4189c52a6f1d5be268aef783e22774c02ab0625';

let Word = function(word, chosenTrans, possibleTrans) {
    this.word = word;
    this.chosenTrans = chosenTrans;
    this.possibleTrans = possibleTrans;
}
// Sledece dve mape simuliraju bazu podataka
const knownDB = {};
//knownDB['wikipedia'] = new Word('wikipedia', 'vikipedija', ['vikipedija', 'wiki']);
//knownDB['also'] = new Word('also', 'takodje', ['takodje', 'takodje2']);
//knownDB['of'] = new Word('of', 'od', ['od', 'od2', 'od3']);

const yeziqsDB = {};
//yeziqsDB['biggest'] = new Word('biggest', 'najveci', ['najveci', 'najveci2']);
//yeziqsDB['reference'] = new Word('reference', 'referenca', ['referenca', 'referenca2']);

// Nepoznate reci, plavi hajlat
const unknown = {};
// Reci koje se trenutno prikazuju na ekranu
const current = {};

// Tekst <p> elementa na ciji klik ce se selektovana rec oznaciti poznatom.
// Za sada je ciljni jezik samo srpski.
const recognizeWord = 'Знам ову реч.';
// Slicno za trazenje ponudjenih prevoda
const showTrans = 'покажи преводе';

let text = `Wikipedia (/ˌwɪkɪˈpiːdiə/ (About this soundlisten) wik-ih-PEE-dee-ə or /ˌwɪkiˈpiːdiə/ (About this soundlisten) wik-ee-PEE-dee-ə) is a multilingual online encyclopedia created and maintained as an open collaboration project[4] by a community of volunteer editors using a wiki-based editing system.[5] It is the largest and most popular general reference work on the World Wide Web,[6][7][8] and is one of the most popular websites ranked by Alexa as of January 2020.[9] It features exclusively free content and no commercial ads, and is owned and supported by the Wikimedia Foundation, a non-profit organization funded primarily through donations.[10][11][12][13]

Wikipedia was launched on January 15, 2001, by Jimmy Wales and Larry Sanger.[14] Sanger coined its name,[15][16] as a portmanteau of "wiki" (the Hawaiian word for "quick")[17] and "encyclopedia". Initially an English-language encyclopedia, versions of Wikipedia in other languages were quickly developed. With at least 6,035,643 articles,[note 3] the English Wikipedia is the largest of the more than 290 Wikipedia encyclopedias. Overall, Wikipedia comprises more than 40 million articles in 301 different languages[18] and by February 2014 it had reached 18 billion page views and nearly 500 million unique visitors per month.[19]

In 2005, Nature published a peer review comparing 42 hard science articles from Encyclopædia Britannica and Wikipedia and found that Wikipedia's level of accuracy approached that of Britannica,[20] although critics suggested that it might not have fared so well in a similar study of a random sampling of all articles or one focused on social science or contentious social issues.[21][22] The following year, Time magazine stated that the open-door policy of allowing anyone to edit had made Wikipedia the biggest and possibly the best encyclopedia in the world, and was a testament to the vision of Jimmy Wales.[23]

Wikipedia has been criticized for exhibiting systemic bias, for presenting a mixture of "truth, half truth, and some falsehoods",[24] and for being subject to manipulation and spin in controversial topics.[25] Wikipedia has also been criticized for gender bias, particularly on its English-language site, where the dominant majority of editors are male. However, Edit-a-thons have been held to encourage female editors and increase the coverage of women's topics.[26][27] Facebook announced that by 2017 it would help readers detect fake news by suggesting links to related Wikipedia articles. YouTube announced a similar plan in 2018.[28] `;



$(document).ready(function() {
    // Tekst se sece na delove koji se cuvaju u nizu
    const textWords = text.split(' ');
    const textArr = [];
    let curr = textWords[0] + ' ';
    for(let i=1; i<textWords.length; i++) {
        if(i%300 === 0) {
            textArr.push(curr);
            curr = '';
        }
        
        curr += textWords[i] + ' ';
    }
    if(curr !== '')
        textArr.push(curr);

    // Progress bar iznad teksta, zavisan izgled od broja strana trenutne lekcije
    let mainHtml = '<div id=progressBar>';
    const brojStrana = textArr.length;
    for(let i=0; i<brojStrana; i++) {
        mainHtml += '<div class="str" style="width: ' + 100/brojStrana + '%'; 
        // Svi odeljci osim prvog imaju opisanu levu granicu u css fajlu,
        // za prvi se ponistava.
        if(i===0)
            mainHtml += '; border: none';
        mainHtml += '" id="str' + i + '"></div>';
    }
    mainHtml += '</div>';
    
    mainHtml += '<div class="prevNext" id="prevPage">prev</div>';
    mainHtml += '<div id="readPage"></div>';
    mainHtml += '<div class="prevNext" id="nextPage">next</div>';
    $('#main').html(mainHtml);
    $('#readPage').html(parseText(textArr[0]));
    $('#nextPage').text(Object.keys(current).length);
    
    // Za svaku nepoznatu rec trazi prevod od Yandex recnika
    /*
    for(let w in unknown) {
        $.ajax({  type:"POST",
            headers: {"X-HTTP-Method-Override":"GET"},
            url: "https://translate.yandex.net/api/v1.5/tr.json/translate",
            dataType: "jsonp",
            data: { 
                key: YANDEX_API_KEY,
                lang: 'en-sr',
                text: w },
                success: function(result){
                if(!result.error){
                    const trans = result.text[0];
                
                    if(!unknown[w].possibleTrans.includes(trans))
                        unknown[w].possibleTrans.push(trans);
                    unknown[w].chosenTrans = trans;
                    if(!current[w].possibleTrans.includes(trans))
                        current[w].possibleTrans.push(trans);
                    current[w].chosenTrans = trans;
                }
            }
        });
    }*/
    /** 
     * Za svaku rec tj. span se dodaje onClick funkcija.
     * 
     * Implementira prelaz izmedju stanja reci.
    */
    for(let w in current) {
        const tag = 'span.' + w;
        $(tag).click(function() {
            let key = tag.split('.')[1];
            
            // Sama rec, na vrhu sidebara
            let sideHtml = '<p class="word">' + key + '</p>';

            // Ako je poznata ili yeziq, ispisuje se izabran prevod
            sideHtml += '<p id="chosenTrans">' + current[key].chosenTrans + '</p>';
            
            // Nepoznate reci i yeziqe mozemo eksplicitno oznaciti kao poznate
            sideHtml += '<p id="statusChange">' + recognizeWord + '</p>';
            
            // Dugme za ispis mogucih prevoda
            sideHtml += '<p id="showTrans">' + showTrans + '</p>';

            // Moguci prevodi
            for(let i=0; i<current[key].possibleTrans.length; i++) {
                sideHtml += '<p class="possibleTrans" id="trans' + i + '">' +  
                    current[key].possibleTrans[i] + '</p>';
            }
            $('#side').html(sideHtml);

            // Poznate reci su vec tako i oznacene, ne treba 'I know this word.'
            if(knownDB.hasOwnProperty(key))
               $('#statusChange').hide();
            
            /* Ako je rec nepoznata, pokazuje se samo 'I know this word.'
            i moguci prevodi. U suprotnom se sakrivaju i dodeljuje se
            onClick funkcija za njihov prikaz. */
            if(unknown.hasOwnProperty(key)) {
                $('#chosenTrans').hide();
                $('#showTrans').hide();
            }
            else {
                $('p.possibleTrans').hide();
                $('#showTrans').click(() => { $('p.possibleTrans').show(); });
            }

            // onClick funkcija za oznacavanje reci kao poznatom
            // Klik je na 'I know this word.'
            $('#statusChange').click(() => {
                knownDB[key] = new Word(current[key].word, 
                    current[key].chosenTrans, current[key].possibleTrans);
                if(unknown.hasOwnProperty(key)) {
                    $(tag).removeClass('status0');
                    delete unknown[key];
                } else {
                    $(tag).removeClass('status1');
                    delete yeziqsDB[key];
                }
                
                /* Pokazuje se izabran prevod, sakrive se 'I know this word.'
                Pokazuje se 'show translations', dodeljuje odgovarajuca
                onClick funkcija i sakrivaju se moguci prevodi 
                */
                $('#chosenTrans').show();
                $('#statusChange').hide();
                $('#showTrans').show();
                $('#showTrans').click(() => { $('p.possibleTrans').show(); });
                $('p.possibleTrans').hide();
            });

            // Dodaju se onClick funkcije za biranje prevoda i azuriranje ispisa
            for(let i=0; i<current[key].possibleTrans.length; i++)
            $('#trans' + i).click(function () {
                // Bira se prevod a rec postaje yeziq ako vec nije bila
                current[key].chosenTrans = $(this).text();
                yeziqsDB[key] = new Word(current[key].word, 
                    current[key].chosenTrans, current[key].possibleTrans);
                
                /*  Rec je bila nepoznata 
                ili poznata ali se mozda bira drugi prevod
                ili je vec bila yeziq ali je izmenjen prethodni prevod.
                Menja se status reci tj klasa za prikaz ako je potrebno. */
                if(unknown.hasOwnProperty(key)) {
                    $(tag).removeClass("status0");
                    delete unknown[key];
                } else if(knownDB.hasOwnProperty(key))
                    delete knownDB[key];

                $(tag).addClass("status1");

                /* Pokazuje se izabran prevod, sakriva se 'I know this word'
                Pokazuje se 'show translations', dodeljuje odgovarajuca
                onClick funkcija i sakrivaju se moguci prevodi 
                */
                $('#chosenTrans').text(current[key].chosenTrans);
                $('#chosenTrans').show();
                $('#statusChange').show();
                $('#showTrans').show();
                $('#showTrans').click(() => { $('p.possibleTrans').show(); });
                $('p.possibleTrans').hide();
            });
        });
    }
});

/**
* Vraca tekst parsiran u html.
* 
* Parsira se svaka rec i menja <span> elementom odgovarajuce klase,
* "status0" ako je nepoznata, "status1" ako je obelezena, bez klase ako je poznata.
* 
* Svaki <span> za neku rec ce pripadati i klasu imenovanu bas tom reci kako bi se
* kasnijim selektovanjem jednog spana azurirali sve jednake reci na ispisu.
* 
* Ako je rec obelezena/poznata/nepoznata dodaje se u odgovarajucu mapu.
* Na kraju se dodaje u mapu svih reci na ekranu.
*/
function parseText(text) {
   return text.replace(/[a-zA-Z]+/g, function(s) {
       let w = s.toLowerCase();
       let result = '<span class="' + w;

       if(knownDB[w] !== undefined)
           result += '">' + s + '</span>';
       else if(yeziqsDB[w] !== undefined)
           result += ' status1">' + s + '</span>';
       else {
           result += ' status0">' + s + '</span>';
           //unknown[w] = new Word(w, 'prevod(' + w + ')', ['prevod(' + w + ')']);
           unknown[w] = new Word(w, '', []);
       }
       if(current[w] === undefined) {
           if(yeziqsDB[w] !== undefined)
               current[w] = new Word(yeziqsDB[w].word, yeziqsDB[w].chosenTrans,
                   yeziqsDB[w].possibleTrans);
           else if(knownDB[w] !== undefined)
               current[w] = new Word(knownDB[w].word, knownDB[w].chosenTrans,
                   knownDB[w].possibleTrans);
           else
               current[w] = new Word(unknown[w].word, unknown[w].chosenTrans,
                   unknown[w].possibleTrans);
       }

       return result;
   });
}