import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { YeziqComponent } from './yeziq.component';

describe('YeziqComponent', () => {
  let component: YeziqComponent;
  let fixture: ComponentFixture<YeziqComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ YeziqComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(YeziqComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
