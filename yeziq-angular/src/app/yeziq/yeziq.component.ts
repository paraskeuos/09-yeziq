import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { UserService } from '../services/user.service';
import { User } from '../models/user.model';
import { Router } from '@angular/router';
import { WordsService } from '../services/words.service';
import { Word } from '../models/word.model';
import { DictSupportedLangMap, DictionaryQueriesMap} from '../models/supported-languages';
import { CoursesPageMap } from '../models/langmaps';

@Component({
  selector: 'app-yeziq',
  templateUrl: './yeziq.component.html',
  styleUrls: ['./yeziq.component.css']
})
export class YeziqComponent implements OnInit, OnDestroy {

  public user: User;
  public yeziqDB = new Set<Word>();
  @Input()
  public courseId;

  private possibleDictionaries = ['Google Translate', 'WordReference', 'Yandex', 'Bing'];

  constructor(private userService: UserService,
              private router: Router,
              private wordsService: WordsService) {

    }

    public returnUserDictionaries() {
      let supportedDictionaries = [];
      this.possibleDictionaries.forEach( (dictionary) => {
        const recnik = DictSupportedLangMap.get(dictionary);
        if(recnik.includes(this.user.srcLang) && recnik.includes(this.user.targetLang) )
          supportedDictionaries.push(dictionary);
      });
      return supportedDictionaries;
    }

    public returnDictionaryQuery(dictionary: string){
      return DictionaryQueriesMap.get(dictionary);
    }

    public returnCoursesPageMap(){
      return CoursesPageMap;
    }

  ngOnInit() {
    const getUser = this.userService.getUser();
    if (!getUser)
      this.router.navigate(['/']);

    const skupReciKursa = new Set<string>();
    this.courseId.lessons.forEach(lekcija => {
      lekcija.forEach(strana => {
        strana.forEach(token => {
          skupReciKursa.add(token);
        });
      });
    });

    const userSub = getUser.subscribe((user: User) => {
      if (user) {
        this.user = user;
        const wordsSub = this.wordsService.getWords({ id: user._id, targetLang: user.targetLang }).subscribe((words: Word[]) => {
          words.forEach((w: Word) => {
            if (w.status === 1) {
              if(skupReciKursa.has(w.word))
                this.yeziqDB.add(w);
            }
          });
        });
      }
    });

  }

  ngOnDestroy() {

  }

}
