import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Translations } from '../models/translations.model';
import { User } from '../models/user.model';
import { WordsService } from '../services/words.service';
import { Subscription, BehaviorSubject } from 'rxjs';
import { DictSupportedLangMap, DictionaryQueriesMap } from '../models/supported-languages';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ReadingSpaceMap } from '../models/langmaps';

@Component({
  selector: 'app-side',
  templateUrl: './side.component.html',
  styleUrls: ['./side.component.css']
})
export class SideComponent implements OnInit, OnDestroy {

  @Input()
  public langMap;

  @Input('knownDB')
  public knownDB: Map<string, Translations>;
  @Input('yeziqDB')
  public yeziqDB: Map<string, Translations>;
  @Input('currWords')
  public currWords: Map<string, Translations>;

  @Input()
  public user: User;
  @Input()
  public knownWordCount: BehaviorSubject<number>;
  @Input()
  public level: BehaviorSubject<number>;

  // Rec koju je izabrao korisnik kroz komponentu main
  @Input('selectedWord')
  public selectedWord: string = null;

  // Naziv recnika i oblik upita, otvarace se novi prozori
  @Input()
  public dictionaries: Map<string, string>;

  @Input()
  public context: string[];


  // Formular za dodavanje korisnickog prevoda
  public addTranslationForm: FormGroup;

  private activeSubs: Subscription[] = [];

  constructor(private wordsService: WordsService, private formBuilder: FormBuilder) {

    this.addTranslationForm = this.formBuilder.group({
      translation: ['', [Validators.required]]
    });
  }

  // Racuna nivo korisnika u trenutnom jeziku na osnovu broja poznatih reci
  private getLevel(known: number): number {
    let level: number;

    if(known < 200) level = 0;
    else if (known < 500) level = 1;
    else if (known < 1250) level = 2;
    else level = Math.floor(2 + known/2500); // na svakih 2500 (2500 -> 3, 5000 -> 4, ...)

    return level;
  }

  // Dodaje se korisnikov prevod
  public addTranslation(): void {
    if(!this.addTranslationForm.valid) {
      window.alert('Not valid!');
      return;
    }

    this.currWords.get(this.selectedWord.toLowerCase()).userTrans = this.addTranslationForm.get('translation').value;
    this.onPickTranslation(this.addTranslationForm.get('translation').value);

    this.addTranslationForm.reset();
  }

  // Rec se oznacava kao poznata klikom na dugme, azurira se stanje u bazi i broj poznatih reci
  public onKnowIt(): void {
    const word = this.selectedWord.toLowerCase();
    this.currWords.get(word).status = null;

    if (this.yeziqDB.has(word) || !this.knownDB.has(word)) {
      this.knownDB.set(word, new Translations(null, this.currWords.get(word).chosenTrans, this.currWords.get(word).possibleTrans.slice(), this.currWords.get(word).userTrans));

      if (this.yeziqDB.has(word))
        this.yeziqDB.delete(word);
    }

    const body = {
      id: this.user._id,
      targetLang: this.user.targetLang,
      word: word,
      chosenTrans: this.currWords.get(word).chosenTrans,
      possibleTrans: this.currWords.get(word).possibleTrans,
      userTrans: this.currWords.get(word).userTrans
    };

    const sub = this.wordsService.wordIsKnown(body).subscribe(obj => {

      const knownWordCountSub = this.wordsService.getKnownWordCount({ userId: this.user._id, targetLang: this.user.targetLang })
        .subscribe((count: number) => {
          this.knownWordCount.next(count);
          this.level.next(this.getLevel(count));
        });
      this.activeSubs.push(knownWordCountSub);
    });
    this.activeSubs.push(sub);
  }

  // Bira se prevod i rec se oznacava kao yeziq, azurira se baza i broj poznati reci
  public onPickTranslation(translation: string): void {
    const word = this.selectedWord.toLowerCase();
    this.currWords.get(word).chosenTrans = translation;

    if (!this.yeziqDB.has(word)) {
      this.currWords.get(word).status = 'status1';
      this.yeziqDB.set(word, new Translations(this.currWords.get(word).status, this.currWords.get(word).chosenTrans, this.currWords.get(word).possibleTrans.slice(), this.currWords.get(word).userTrans));
    }
    else
      this.yeziqDB.get(word).chosenTrans = this.currWords.get(word).chosenTrans;

    if (this.knownDB.has(word))
      this.knownDB.delete(word);

    const body = {
      id: this.user._id,
      targetLang: this.user.targetLang,
      word: word,
      chosenTrans: this.currWords.get(word).chosenTrans,
      possibleTrans: this.currWords.get(word).possibleTrans,
      userTrans: this.currWords.get(word).userTrans
    };

    const sub = this.wordsService.addYeziq(body).subscribe(obj => {

      const knownWordCountSub = this.wordsService.getKnownWordCount({ userId: this.user._id, targetLang: this.user.targetLang })
        .subscribe((count: number) => {

          this.knownWordCount.next(count);
          this.level.next(this.getLevel(count));
        });
      this.activeSubs.push(knownWordCountSub);
    });
    this.activeSubs.push(sub);
  }

  public generateQueryString(): string{
    let queryString = '';

    this.context.forEach((word) => {queryString += (word + ' '); });

    return queryString;
  }

  public returnContextInLanguage(){
    return ReadingSpaceMap.get(this.user.srcLang).get('context');
  }

  ngOnInit() {

  }

  ngOnDestroy() {
    this.activeSubs.forEach(sub => {
      sub.unsubscribe();
    });
  }
}
