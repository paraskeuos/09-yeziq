import { Component } from '@angular/core';
import { Translations } from './models/translations.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'yeziq-angular';
}
