import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { MainComponent } from './main/main.component';
import { SideComponent } from './side/side.component';
import { ReadingSpaceComponent } from './reading-space/reading-space.component';
import { CoursesPageComponent } from './courses-page/courses-page.component';
import { LoginPageComponent } from './login-page/login-page.component';
import { LoadingCircleComponent } from './loading-circle/loading-circle.component';
import { YeziqComponent } from './yeziq/yeziq.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    MainComponent,
    SideComponent,
    ReadingSpaceComponent,
    CoursesPageComponent,
    LoginPageComponent,
    LoadingCircleComponent,
    YeziqComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
