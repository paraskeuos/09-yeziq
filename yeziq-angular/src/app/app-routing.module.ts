import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CoursesPageComponent } from './courses-page/courses-page.component';
import { ReadingSpaceComponent } from './reading-space/reading-space.component';
import { LoginPageComponent } from './login-page/login-page.component';
import { YeziqComponent } from './yeziq/yeziq.component';


const routes: Routes = [
    { path: '', component: LoginPageComponent },
    { path: 'courses', component: CoursesPageComponent },
    { path: 'read', component: ReadingSpaceComponent },
    { path: 'yeziq', component: YeziqComponent },
    { path: '**', redirectTo: '/', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    useHash: true
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
