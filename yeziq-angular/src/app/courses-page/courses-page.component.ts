import { Component, OnInit, OnDestroy, ViewChild, ElementRef, EventEmitter } from '@angular/core';
import { Subscription, BehaviorSubject, Observable } from 'rxjs';
import { UserService } from '../services/user.service';
import { User } from '../models/user.model';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { FormBuilder, FormGroup, Validators, ValidatorFn, AbstractControl } from '@angular/forms';
import { Course } from '../models/course.model';
import { CoursesPageMap } from '../models/langmaps';
import { WordsService } from '../services/words.service';
import { RegexMap } from '../models/supported-languages';
import { LessonStats } from '../models/lesson-stats.model';
import { Word } from '../models/word.model';


@Component({
  selector: 'app-courses-page',
  templateUrl: './courses-page.component.html',
  styleUrls: ['./courses-page.component.css']
})
export class CoursesPageComponent implements OnInit, OnDestroy {

  // Mapa sa regularnim izrazima, potrebna za kreiranje kurseva
  private regexMap = RegexMap;
  // Mapa sa natpisima u zavisnosti od izvornog jezika
  public langMap = CoursesPageMap;

  // Prijavljeni korisnik
  public user: User;

  // Trenutan broj poznatih reci u trenutnom ciljnom jeziku, prikazuje se u navbaru.
  // Vrednost se azurira pri otvaranju stranice za kurseve a eventualno se azurira dok
  // korisnik cita tekstove i uci nove reci
  public knownWordCount = new BehaviorSubject<number>(-1);

  // Nivo za trenutni jezik, zavisi od broja poznatih reci, prikaz u navbaru
  public level = new BehaviorSubject<number>(-1);

  // Kursevi za trenutno izabran ciljni jezik. Sadrzaj se menja ako korisnik u navbaru
  // promeni ciljni jezik.
  public courses: BehaviorSubject<Course[]> = new BehaviorSubject<Course[]>(null);
  // Mapa sa statistikom za svaku lekciju, kljuc je id kursa.
  public coursesStats: Map<string, Array<LessonStats>>;

  // U HTML sablonu prikazuje ili sklanja opcije za potvrdu brisanja kursa.
  public showRemoveOpts: boolean[];

  // Formular za kreiranje kursa
  public courseTextForm: FormGroup;
  // Toggle za prikaz dela za kreiranje kurseva
  public showAddCourse: boolean = false;

  // Da li je u toku kreiranje kursa
  public loading: boolean = false;
  // Opis ispod animacije za ucitavanje/kreiranje kurseva
  public loadingProgress: string = '';

  // Input za fajlove
  @ViewChild('fileInput', { static: false })
  public fileInput: ElementRef;

  // Prikaz greske pri pravljenju kursa
  public addCourseError: string = null;

  // Togle za prikaz lekcija odabranog kursa
  public showLessonList: boolean = false;
  // Ako se trazi lista lekcija za odredjeni kurs iz komponente reading-space
  public courseId: string;

  // Indeks izabranog kursa
  public courseIndex: number;

  private activeSubs: Subscription[] = [];

  public showYeziq = false;

  constructor(private userService: UserService,
    private router: Router,
    private routes: ActivatedRoute,
    private wordsService: WordsService,
    private formBuilder: FormBuilder) {

    // Ako je izvrsen login, getUser nece biti null.
    // Sprecava direktno pristupanje /courses delu pre logina.
    const getUser = this.userService.getUser();
    if (!getUser)
      this.router.navigate(['/']);

    // Ako se direktno trazi lista lekcija za odredjeni kurs iz komponente reading-space
    this.routes.paramMap.subscribe(params => {
      this.courseId = params.get('course');
      if (this.courseId) {
        this.showLessonList = true;
      }
    });

    // Inace se ucitava korisnik, osvezavaju kursevi, broj poznatih reci
    const userSub = getUser.subscribe((user: User) => {
      if (user) {
        this.user = user;

        this.refreshCourses();

        const knownWordCountSub = this.wordsService.getKnownWordCount({ userId: this.user._id, targetLang: this.user.targetLang })
          .subscribe((count: number) => {

            this.knownWordCount.next(count);
            this.level.next(this.getLevel(count));
          });
        this.activeSubs.push(knownWordCountSub);
      }
    });

    this.activeSubs.push(userSub);

    this.courseTextForm = this.formBuilder.group({
      name: ['', [Validators.required, Validators.maxLength(30)]],
      file: [''],
      text: ['']
    }, { validator: this.contentValidator() });
  }

  /**
   *  Korisnik preko navbara menja ciljni jezik.
   *
   *  Prvo se u bazi isti promeni (targetLang) kako bi po sledecem loginu prvo bili
   *  ucitani kursevi u poslednjem izabranom ciljnom jeziku.
   *
   *  Potom se osvezavaju kursevi i broj poznatih reci, po istom principu kao u konstruktoru.
   */
  public onNewTargetLang(newTargetLang: string): void {
    const changeUserSub = this.userService.changeTargetLang({ id: this.user._id, newTargetLang: newTargetLang }).subscribe((user: User) => {
      this.user = user;
      this.showAddCourse = false;
      this.showLessonList = false;
      this.showYeziq = false;
      this.refreshCourses();

      const knownWordCountSub = this.wordsService.getKnownWordCount({ userId: this.user._id, targetLang: this.user.targetLang })
        .subscribe((count: number) => {

          this.knownWordCount.next(count);
          this.level.next(this.getLevel(count));
        });
      this.activeSubs.push(knownWordCountSub);
    });

    this.activeSubs.push(changeUserSub);
  }

  // Klikom na kurs se bira njegov indeks i prikazuju se lekcije istog
  public showLessons(i: number): void {
    this.courseIndex = i;
    this.showLessonList = true;
  }

  // Racuna nivo korisnika u trenutnom jeziku na osnovu broja poznatih reci
  private getLevel(known: number): number {
    let level: number;

    if (known < 200) level = 0;
    else if (known < 500) level = 1;
    else if (known < 1250) level = 2;
    else level = Math.floor(2 + known / 2500); // na svakih 2500 (2500 -> 3, 5000 -> 4, ...)

    return level;
  }

  public getCourseStats(courseId: string): string {
    if(!this.coursesStats.get(courseId)) {
        return '';
    }

    let unknown = 0;
    let total = 0;

    this.coursesStats.get(courseId).forEach((stats: LessonStats) => {
      unknown += stats.getUnknownCount();
      total += stats.getTotal();
    });

    if(unknown === 0) {
      return this.langMap.get(this.user.srcLang).get('allWordsKnownByCourse');
    }

    let unknownPct = Math.floor(unknown/total*100);
    // Ako broj nepoznatih nije 0 ali se procenat zaokruzi na 0%, postavlja se na 1%
    unknownPct = unknownPct === 0 ? 1 : unknownPct;

    return `${unknownPct}% ${this.langMap.get(this.user.srcLang).get('unknownPctByCourse')}`;
  }

  public getStyleIfComplete(courseId: string) {
    if(!this.coursesStats.get(courseId))
      return {};

    for(let i=0; i<this.coursesStats.get(courseId).length; i++) {
      if(this.coursesStats.get(courseId)[i].getUnknownCount() > 0)
        return {};
    }

    return { color: 'rgb(0, 210, 0)' };
  }

  // Osvezavanje niza kurseva
  private refreshCourses(): void {
    this.loadingProgress = this.langMap.get(this.user.srcLang).get('loadingCoursesDesc');
    this.courses.next(null);
    this.loading = false;

    const sub = this.userService.getCoursesByAuthor({ author: this.user.username, targetLang: this.user.targetLang }).subscribe((courses: Course[]) => {

      this.courses.next(courses);
      this.showRemoveOpts = new Array(courses.length);

      // Informacije o poznatim ("belim") i yeziq ("zutim") recima iz baze
      this.coursesStats = new Map<string, Array<LessonStats>>();
      const knownDB = new Set<string>();
      const yeziqDB = new Set<string>();

      const regex = new RegExp('^' + this.regexMap.get(this.user.targetLang) + '$');

      const wordsSub = this.wordsService.getWords({ id: this.user._id, targetLang: this.user.targetLang })
      .subscribe((words: Word[]) => {
        // Rec se ubacuje u odgovarajuci skup
        words.forEach((w: Word) => {
          if (w.status === 1) {
            yeziqDB.add(w.word);
          } else {
            knownDB.add(w.word);
          }
        });

        // Za svaku lekciju svakog kursa se racuna statistika o recima
        this.courses.value.forEach((course: Course) => {

          this.coursesStats.set(course._id, new Array<LessonStats>(course.lessons.length));
          for (let i = 0; i < course.lessons.length; i++) {
            const unknown = new Set<string>();
            const yeziq = new Set<string>();
            const total = new Set<string>();

            course.lessons[i].forEach(page => {
              page.filter(token => regex.test(token)).forEach(w => {
                const word = w.toLowerCase();

                if (yeziqDB.has(word)) {
                  yeziq.add(word);
                }
                else if (!knownDB.has(word)) {
                  unknown.add(word);
                }

                total.add(word);
              });
            });

            let unknownPct = Math.floor(unknown.size / total.size * 100);
            // Ako broj nepoznatih nije 0 ali se procenat zaokruzi na 0%, postavlja se na 1%
            unknownPct = unknown.size !== 0 && unknownPct === 0 ? 1 : unknownPct;

            this.coursesStats.get(course._id)[i] = new LessonStats(unknown.size, unknownPct, yeziq.size, total.size);
          };
        });

        // Ako se doslo iz komponente reading-space sa zahtevom za povratak na listu lekcija odredjenog kursa
        // ili ako je upravo kreiran kursa, izlistavaju se njegove lekcije
        if (this.courseId) {
          for (let i = 0; this.courses.getValue().length; i++) {
            if (this.courses.getValue()[i]._id === this.courseId) {
              this.showLessons(i);

              break;
            }
          }
        }


        this.loading = false;
      });

      this.activeSubs.push(wordsSub);
    });

    this.activeSubs.push(sub);
  }

  // U formularu za kreiranje kursa se mora uneti ili tekst ili fajl.
  public contentValidator(): ValidatorFn {
    return () => {
      if (!this.courseTextForm)
        return null;

      const contentEntered = (this.courseTextForm.get('file').value !== '') || (this.courseTextForm.get('text').value !== '');
      return contentEntered ? null : { noContent: true };
    }
  }

  // Kada se izabere fajl, prikazuje se samo njegovo ime, ne njegova apsolutna putanja
  public getChosenFile(): string {
    const absPath = this.courseTextForm.get('file').value;
    if (!absPath)
      return '';

    return absPath.substr(absPath.lastIndexOf('\\') + 1);
  }

  // Pokrece se kreiranje i cuvanje kursa na osnovu formulara
  public submitText(data) {
    this.addCourseError = null;
    if (!this.courseTextForm.valid) {
      if(this.courseTextForm.get('name').errors.maxlength)
        this.addCourseError = this.langMap.get(this.user.srcLang).get('addCourseFormNameTooLong');
      else
        this.addCourseError = this.langMap.get(this.user.srcLang).get('addCourseFormErrMsg');

      return;
    }

    // Loading animacija dok se kreira kurs.
    this.loadingProgress = this.langMap.get(this.user.srcLang).get('creatingCourseDesc');;
    this.loading = true;

    // Automatski ce se izlistati lekcije kreiranog kursa
    this.showLessonList = true;

    // Ako je izabran fajl, kreira se kurs iz njega, cak i ako je unesen i tekst.
    if (data.file) {
      const file = this.fileInput.nativeElement.files[0];

      // .txt fajlovi
      if (data.file.endsWith('.txt')) {
        const reader = new FileReader();

        reader.onerror = () => {
          this.loading = false;
          this.addCourseError = this.langMap.get(this.user.srcLang).get('addCourseErrMsg');
          this.courseTextForm.reset();
        };

        // Ako se uspesno procita fajl, kreira se kurs, pamti u bazi i osvezava se niz kurseva
        reader.onload = () => {
          const course = this.makeCourse(<string>reader.result, data.name, this.user.username, this.user.targetLang);

          this.loadingProgress = this.langMap.get(this.user.srcLang).get('savingCourseDesc');
          const sub = this.userService.createCourse(course).subscribe((obj) => {
            if (obj.message === 'saved') {
              this.courses.next(null);
              this.toggleViews();

              // Da bi se automatski izlistale lekcije kreiranog kursa
              this.courseId = obj.courseId;
              this.refreshCourses();
            } else {
              this.loading = false;
              this.showLessonList = false;
              this.addCourseError = this.langMap.get(this.user.srcLang).get('addCourseSaveErrMsg');
            }
          });

          this.activeSubs.push(sub);
          this.courseTextForm.reset();
        }

        reader.readAsText(file, 'UTF-8');
      } else if (data.file.endsWith('.pdf')) {

        // Salje se ceo fajl serveru koji vraca tekstualni oblik fajla.
        // Potom se sve izvrsava kao u prethodnom slucaju
        let formData = new FormData();
        formData.append('file', file, file.name);

        const pdfSub = this.userService.getTextFromPdf(formData).subscribe((text: string) => {
          if (text) {
            const course = this.makeCourse(text, data.name, this.user.username, this.user.targetLang);

            this.loadingProgress = this.langMap.get(this.user.srcLang).get('savingCourseDesc');
            const sub = this.userService.createCourse(course).subscribe((obj) => {
              if (obj.message === 'saved') {
                this.courses.next(null);
                this.toggleViews();
                // Da bi se automatski izlistale lekcije kreiranog kursa
                this.courseId = obj.courseId;
                this.refreshCourses();
              } else {
                this.loading = false;
                this.showLessonList = false;
                this.addCourseError = this.langMap.get(this.user.srcLang).get('addCourseSaveErrMsg');
              }
            });

            this.activeSubs.push(sub);
            this.courseTextForm.reset();
          } else {
            this.loading = false;
            this.showLessonList = false;
            this.addCourseError = this.langMap.get(this.user.srcLang).get('addCourseErrMsg');
          }
        });
        this.activeSubs.push(pdfSub);
      } else if (data.file.endsWith('.epub')) {

        // Poput .pdf fajla, salje se fajl serveru ali se dobija tekst u xml formatu.
        let formData = new FormData();
        formData.append('file', file, file.name);

        const epubSub = this.userService.getTextFromEpub(formData).subscribe((text: string) => {
          if (text) {
            const course = this.makeCourse(this.xmlToText(text), data.name, this.user.username, this.user.targetLang);

            this.loadingProgress = this.langMap.get(this.user.srcLang).get('savingCourseDesc');
            const sub = this.userService.createCourse(course).subscribe((obj) => {
              if (obj.message === 'saved') {
                this.courses.next(null);
                this.toggleViews();

                // Da bi se automatski izlistale lekcije kreiranog kursa
                this.courseId = obj.courseId;
                this.refreshCourses();
              } else {
                this.loading = false;
                this.showLessonList = false;
                this.addCourseError = this.langMap.get(this.user.srcLang).get('addCourseSaveErrMsg');
              }
            });

            this.activeSubs.push(sub);
            this.courseTextForm.reset();
          } else {
            this.loading = false;
            this.addCourseError = this.langMap.get(this.user.srcLang).get('addCourseErrMsg');
          }
        });
        this.activeSubs.push(epubSub);
      }

    } else { // Kreiranje kursa na osnovu teksta iz formulara

      const course = this.makeCourse(data.text, data.name, this.user.username, this.user.targetLang);

      this.loadingProgress = this.langMap.get(this.user.srcLang).get('savingCourseDesc');
      const sub = this.userService.createCourse(course).subscribe((obj) => {
        if (obj.message === 'saved') {
          this.courses.next(null);
          this.toggleViews();

          // Da bi se automatski izlistale lekcije kreiranog kursa
          this.courseId = obj.courseId;
          this.refreshCourses();
        } else {
          this.loading = false;
          this.showLessonList = false;
          this.addCourseError = this.langMap.get(this.user.srcLang).get('addCourseSaveErrMsg');
        }
      });

      this.activeSubs.push(sub);
      this.courseTextForm.reset();
    }
  }

  // Brisanje kursa iz baze klikomna odgovarajucu slicicu
  public deleteCourse(id: string): void {

    this.courses.next(null);
    const sub = this.userService.deleteCourse(id).subscribe(obj => {

      this.refreshCourses();
    });
    this.activeSubs.push(sub);
  }

  /**
   * Procitan epub fajl stize u xml formatu.
   * Pretprocesira su u obican text.
   * Svi heading tagovi se konvertuju u dvostruki novi red
   * dok se brisu svi ostali tagovi i specijalni znaci (npr. &nbsp)
   */
  private xmlToText(text: string): string {
    return text.replace(/<[/]?[ ]*h[1-6][ ]*>/g, '\n\n')
      .replace(/(&[a-z]+;)|(<[/]?[^>]*[/]?>)/g, '')
  }

  /**
   * Pravi novi kurs na osnovu teksta.
   */
  private makeCourse(text: string, courseName: string, author: string, targetLang: string): Course {
    const course: Course = {
      _id: '',
      name: courseName,
      author: author,
      targetLang: targetLang,
      lessons: [],
      unknownWords: 0,
      yeziqs: 0
    };

    /**
     * I validne reci i nevalidni tokeni se cuvaju u nizu, odrzava se redosled iz teksta.
     * Validne reci se ogranicavaju tagom <split> tako da ce poziv split('<split>')
     * zahvatiti i validne reci i nevalidne tokene u pravilnom redosledu.
     * */
    const regex = new RegExp(this.regexMap.get(this.user.targetLang), 'g');
    let textArr = text.replace(regex, w => '<split>' + w + '<split>')
      .replace(/\n[ \t]*\n/g, '<split><nl><split>')
      .replace(/[ ]+/g, ' ')
      .replace(/\t/g, '<split><tab><split>')
      .replace(/\n/g, '')
      .split('<split>')
      .filter(w => w !== '');

    // Na kraju se niz reci deli na stranice
    let lessonInd = 0;
    let page = 0;

    // Broj tokena po stranici se bira dinamicki jer <nl> rusi balans
    const avgRows = 20;
    const avgLine = 50;
    const avgTokensPerPage = avgRows * avgLine;

    while (textArr.length) {

      if (page === 0)
        course.lessons[lessonInd] = [];

      let currPageSize = 0;
      for (let i = 0; i < textArr.length; i++) {

        if (textArr[i] === '<nl>') {
          if (currPageSize >= avgTokensPerPage - 8 * avgLine)
            break;

          currPageSize += 8 * avgLine;
        } else {
          if (currPageSize >= avgTokensPerPage)
            break;
          currPageSize += 1;
        }
      }
      course.lessons[lessonInd].push(textArr.splice(0, currPageSize));

      page++;

      if (page === 20) {
        page = 0;
        lessonInd++;
      }
    }

    return course;
  }

  // Toggle izlistavanje kurseva / formular za kreiranje kursa
  public toggleViews(): void {
    this.showAddCourse = !this.showAddCourse;
  }

  public backToCourse(): void {
    this.showYeziq = false;
    this.showLessonList = false;
    this.courseId = null;
  }

  public yeziq(): void{
    this.showYeziq = true;
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    this.activeSubs.forEach((sub: Subscription) => {
      sub.unsubscribe();
    });
  }
}
