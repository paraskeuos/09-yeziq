export class Translations {
     constructor(public status: string, 
                public chosenTrans: string, 
                public possibleTrans: Array<string>,
                public userTrans: string) {
    }
}