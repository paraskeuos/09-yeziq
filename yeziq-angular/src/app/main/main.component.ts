import { Component, OnInit, Input, Output, OnDestroy } from '@angular/core';
import { EventEmitter } from '@angular/core';
import { Translations } from '../models/translations.model';
import { Course } from '../models/course.model';
import { Subscription, Observable, BehaviorSubject } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from '../services/user.service';
import { User } from '../models/user.model';
import { Word } from '../models/word.model';
import { WordsService } from '../services/words.service';
import { ReadingSpaceMap } from '../models/langmaps';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit, OnDestroy {

  public langMap = ReadingSpaceMap;

  // Reference na odgovarajuce mape reci
  @Input()
  public knownDB: Map<string, Translations>;
  @Input()
  public yeziqDB: Map<string, Translations>;
  @Input()
  public currWords: Map<string, Translations>;

  @Input()
  public user: User;
  @Input()
  public knownWordCount: BehaviorSubject<number>;
  @Input()
  public level: BehaviorSubject<number>;

  @Input()
  public course: Course;
  @Input()
  public currPage: Array<string>;
  @Input()
  private currPageInd: number;
  @Input()
  public currLessonInd: number;

  // selectedWord ce koristiti side komponenta za prikaz kliknute reci u tekstu
  @Output('selectedWord')
  public emitSelectedWord: EventEmitter<string> = new EventEmitter<string>();

  @Output('context')
  public emitContext: EventEmitter<string[]> = new EventEmitter<string[]>();

  private activeSubs: Subscription[] = [];

  constructor(private wordsService: WordsService, private router: Router) { }

  // Racuna nivo korisnika u trenutnom jeziku na osnovu broja poznatih reci
  private getLevel(known: number): number {
    let level: number;

    if (known < 200) level = 0;
    else if (known < 500) level = 1;
    else if (known < 1250) level = 2;
    else level = Math.floor(2 + known / 2500); // na svakih 2500 (2500 -> 3, 5000 -> 4, ...)

    return level;
  }

  /** Ako se prelazi na sledecu stranu, onda se i sve nepoznate reci na trenutnoj
  /*  pamte kao poznate. Azurira se baza i broj poznatih reci.
  /*  Ako je poslednja strana, odlazi se na stranu za kurseve.
  */
  public onNextPage(): void {
    this.allUnknownToKnown(this.currPage);

    // Poslednja strana u lekciji
    if (this.currPageInd === this.course.lessons[this.currLessonInd].length - 1) {

      // Ako nije poslednja lekcija, bira se sledeca lekcija i prva strana, inace je zavrsen kurs
      if (this.currLessonInd < this.course.lessons.length - 1) {
        this.currPageInd = 0;
        this.currLessonInd++;
        this.currPage = this.course.lessons[this.currLessonInd][0];
      } else
        this.router.navigate(['courses']);

    } else
      this.currPage = this.course.lessons[this.currLessonInd][++this.currPageInd];
  }

  // Prelazak na prethodnu stranicu osim ako je trenutna stranica prva u lekciji.
  public onPrevPage(): void {
    if (this.currPageInd === 0)
      return;

    this.currPage = this.course.lessons[this.currLessonInd][--this.currPageInd];
  }

  private punctuationContainingFunction(word: string): boolean{
    let punctuationSigns = [
      '?', '.', '!', ',', ';', '\"', '«', '»'
    ];

    let flag = false;
    punctuationSigns.forEach( (sign) => {
      if(word.includes(sign))
        flag = true;
    } )

    return flag;
  }

  private getArrayOfContextWords(index: number): string[]{
    let arrayOfContextWords = [];

    let pomIndex = index - 1;
    while (pomIndex > 0 && pomIndex <= this.currPage.length ){
      const word: string = this.currPage[pomIndex].trim();
      if(this.punctuationContainingFunction(word) == true)
        break;
      if(this.currPage[pomIndex]!==" "){
        arrayOfContextWords.push(this.currPage[pomIndex]);
      }
      pomIndex--;
    }

    arrayOfContextWords.reverse();
    arrayOfContextWords.push(this.currPage[index]);

    pomIndex = index + 1;
    while (pomIndex > 0 && pomIndex <= this.currPage.length ){
      const word: string = this.currPage[pomIndex].trim();
      if(this.punctuationContainingFunction(word) == true)
        break;


      if(this.currPage[pomIndex]!==' '){
        arrayOfContextWords.push(this.currPage[pomIndex]);
      }
      pomIndex++;
    }
    return arrayOfContextWords.filter( (word) => word!=='<nl>' && word!=='<tab>' );
  }

  public onClickWord(word: string, index: number): void {
    const w = word.toLowerCase();

    // Ako jos nije konsultovan recnik za doticnu rec, to znaci da je rec ili nepoznata,
    // ili je prethodno postala poznata bez klika na nju, odnosno prelaskom na sledecu stranu.
    // Recniku se salje GET zahtev i cuvaju prevodi. Inace se samo aktivira prikaz u sidebar-u.
    // Dodatno, ako je rec bila nepoznata, bira se prvi prevod kao izabran i rec postaje yeziq.

    /**
     *  NAPOMENA
     *  Sada usluge Yandex recnika nisu besplatne pa sa servera uvek stize odgovor null.
     */

    if (this.currWords.get(w).possibleTrans.length === 0) {
      const body = {
        lang: [this.user.targetLang, this.user.srcLang].join('-'),
        text: w
      };
      const sub = this.wordsService.getTranslations(body).subscribe(translations => {

        if (translations) {
          this.currWords.get(w).possibleTrans = translations;

          if (!this.knownDB.has(w)) {
            this.currWords.get(w).status = 'status1';
            this.currWords.get(w).chosenTrans = this.currWords.get(w).possibleTrans[0];
            this.yeziqDB.set(w, new Translations(this.currWords.get(w).status, this.currWords.get(w).chosenTrans, this.currWords.get(w).possibleTrans.slice(), this.currWords.get(w).userTrans));

            const yeziqBody = {
              id: this.user._id,
              targetLang: this.user.targetLang,
              word: word,
              chosenTrans: this.currWords.get(word).chosenTrans,
              possibleTrans: this.currWords.get(word).possibleTrans,
              userTrans: this.currWords.get(word).userTrans
            };

            const yeziqSub = this.wordsService.addYeziq(yeziqBody).subscribe(obj => { });

            this.activeSubs.push(yeziqSub);
          }
        }
      });

      this.activeSubs.push(sub);
    }
    //this.emitSelectedWord.emit(word);
    let context = this.getArrayOfContextWords(index);
    context.push(word);
    this.emitContext.emit(context);
  }

  // Dobijanje stila za odsecke u progress baru.
  public getStyle(ind: number): any {
    const first = ind === 0;
    const last = ind === this.course.lessons[this.currLessonInd].length - 1;
    const border = '1px solid';
    const radius = '20px';

    return {
      width: (100 / this.course.lessons[this.currLessonInd].length).toString() + '%',
      'border-top': border,
      'border-bottom': border,
      'border-left': border,
      'border-top-left-radius': first ? radius : 'default',
      'border-bottom-left-radius': first ? radius : 'default',
      'border-right': last ? border : 'default',
      'border-top-right-radius': last ? radius : 'default',
      'border-bottom-right-radius': last ? radius : 'default',
      'background-color': ind === this.currPageInd ? 'rgb(230, 230, 230)' : 'inherit'
    };
  }

  // Sve nepoznate reci na stranici se oznacavaju kao poznate.
  private allUnknownToKnown(page: Array<string>): void {
    // U niz words se stavljaju sve nepoznate reci na stranici
    const words = [];
    page.forEach(word => {
      word = word.toLowerCase();
      if (this.knownDB.has(word) || this.yeziqDB.has(word) || !this.currWords.has(word))
        return;

      this.knownDB.set(word, new Translations('', this.currWords.get(word).chosenTrans, this.currWords.get(word).possibleTrans.slice(), this.currWords.get(word).userTrans));
      this.currWords.get(word).status = '';
      words.push({
        word: word,
        chosenTrans: this.currWords.get(word).chosenTrans,
        possibleTrans: this.currWords.get(word).possibleTrans,
        userTrans: this.currWords.get(word).userTrans
      });
    });

    // Telo zahteva
    const body = {
      id: this.user._id,
      targetLang: this.user.targetLang,
      words: words
    };
    // Azurira se stanje u bazi, takodje se osvezava broj poznatih reci (prikaz u navbaru)
    const sub = this.wordsService.wordsOnPageAreKnown(body).subscribe(obj => {

      const knownWordCountSub = this.wordsService.getKnownWordCount({ userId: this.user._id, targetLang: this.user.targetLang })
        .subscribe((count: number) => {

          this.knownWordCount.next(count);
          this.level.next(this.getLevel(count));
        });
      this.activeSubs.push(knownWordCountSub);
    });
    this.activeSubs.push(sub);
  }

  ngOnInit() { }

  ngOnDestroy() {
    this.activeSubs.forEach(sub => sub.unsubscribe());
  }
}
