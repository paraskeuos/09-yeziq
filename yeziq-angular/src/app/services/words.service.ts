import { Injectable } from '@angular/core';
import { Word } from '../models/word.model';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class WordsService {

  //private yandexDictUrl = 'https://dictionary.yandex.net/api/v1/dicservice.json/lookup';
  private yandexTranslateUrl = 'https://translate.yandex.net/api/v1.5/tr.json/translate';

  private wordsArray: Observable<Word[]>;
  private wordsUrl = `http://${environment.server}:${environment.serverPort}/words/`;

  constructor(private http: HttpClient) {}

  public getWords(data): Observable<Word[]> {
    this.wordsArray = this.http.post<Word[]>(this.wordsUrl, data);
    return this.wordsArray;
  }

  public getKnownWordCount(data): Observable<number> {
    return this.http.post<number>(this.wordsUrl + 'count', data);
  }

  public wordIsKnown(data): Observable<any> {
    return this.http.post<any>(this.wordsUrl + 'knownOne', data);
  }

  public wordsOnPageAreKnown(data): Observable<any> {
    return this.http.post<any>(this.wordsUrl + 'knownMany', data);
  }

  public addYeziq(data): Observable<any> {
    return this.http.post<any>(this.wordsUrl + 'yeziq', data);
  }

  public getTranslations(data): Observable<Array<string>> {
    return this.http.post<Array<string>>(this.wordsUrl + '/translate', data);
  }
}
