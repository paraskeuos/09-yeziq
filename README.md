
# Project 09-Yeziq

Učenje stranih jezika čitanjem tekstova uz automatizovano konsultovanje online rečnika.

## Developers

- [Milan Radišić, 192/2016](https://gitlab.com/paraskeuos)
- [Nemanja Živanović, 89/2016](https://gitlab.com/NemanjaZivanovic)
- [Aleksa Voštić, 168/2016](https://gitlab.com/AKile9V)
- [Stefan Isailović, 29/2016](https://gitlab.com/Isa1997)
